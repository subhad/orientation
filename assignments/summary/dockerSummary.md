### Docker Basics
***

#### What is Docker
***
* Docker is a program for developers to develop and run applications with containers.

#### Why to use Docker(a brief History)
***
* When a software is made through various stages(like development, testing, operating etc) it has to be run on different systems.
* But due to some dependency issues it doesn't perform well in every system.
* Then came **Hypervisors**, who solved this a lot using _Guest OS_s(just like _Virtual Machine_).
* But this process resulted wastage of RAM and Memory.
* Finaly **_Docker_** came up replacing _Hypervisors_ and used **_containers_** instead of _Guest OS_.

	That's how _Docker_ came into picture.

	![Docker Architecture](https://thinkpalm.com/wp-content/uploads/2018/03/Docker-Engine.jpg)  
  ( Docker Architecture Diagram )
#### Docker Terminologies
***

##### Docker Image
***
* When we install OS for VM, we install its image(.iso file). Just like that Docker has image.
* It contains everything to develop and run an application :
  * code
  * runtime
  * libraries
  * environment variables
  * configuration files

##### Container 
***
* It is just an running Docker image.

##### Docker Hub
***
* Docker Hub is like GitHub but for docker images and containers.

#### Docker Basic Commands
***

* **_docker ps_** command allows us to view all the containers that are running on the _Docker Host_.
* **_docker start_** command starts a container.
* **_docker stop_** command stops a running container.
* **_docker run_** command creates container from a docker image.
* **_docker rm_** command removes/deletes the container.

#### Common Operations on Docker
***

1. Downloading/pulling the docker images wanted to be worked with.
2. Copying code inside the docker
3. Accessing docker terminal
4. Installing additional required dependencies
5. Compile and Run the Code inside docker
6. Document steps to run program in README.md file
7. Commit the changes done to the docker.
8. Push docker image to the docker-hub and share repository with people who want to try the code.

