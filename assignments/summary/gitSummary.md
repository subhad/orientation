### Git Basics  
***
  
#### What is Git  
***
* Git is a _version-control system_ for tracking changes in computer files and coordinating work on those files among multiple people.  
or  
Git is a **Distributed Version Control System**.
* Git enables each member endolged in the project, to have their own copy(clone) of the project, which makes the development of the project faster and easier.
* It also helps _synchronising codes_ between multiple people.

#### Git Terminologies
***

##### Repositories(Repo) :  
***
* It is the place or directory, where all the files and folders of the project are stored.

##### Branch :  
***
* If project is a tree, then the parts of the codes given to different members are called _Branches_ of the project.

##### GitLab:
***
* The 2nd most popular remote storage solution for git repos.

##### Merge:
***
* When a branch is ready to become part of the primary codebase, it will get merged into the master branch. Merging means integrating two branches together.

##### Clone:
***
* Cloning a repo takes the entire online repository and makes an exact copy of it on your local machine.

##### Fork:
***
* Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.

#### Git Workflow 
***
* A file in the **Working Directory** can exist in three possible states :

1. **Staged :** This means the files with the updated changes are marked to be committed to the local repository, but not yet committed.
2. **Modified :** This means the files with the updated changes are not yet stored in the local repository.
3. **Committed :** This means that the changes you made to your file are safely stored in the local repository.

* There are four fundamental elements in the Git Workflow : Working Directory, Staging Area, Local Repository and Remote Repository.
* Here is a diagram representing the Git Workflow :  
	
	![Git Workflow](https://i.stack.imgur.com/eXlL8.png)

### Some Git Basic Commands
***
* **_git checkout_**  command is used to switch between branches in a repository.  
* **_git add_**  is a command used to add a file that is in the working directory to the staging area.  
* **_git commit_**  is a command used to add all files that are staged to the local repository.  
* **_git push_**  is a command used to add all committed files in the local repository to the remote repository. So in the remote repository, all files and changes will be visible to anyone with access to the remote repository.  
* **_git fetch_**  is a command used to get files from the remote repository to the local repository but not into the working directory.  
* **_git merge_**  is a command used to get the files from the local repository into the working directory.  
* **_git pull_**  is command used to get files from the remote repository directly into the working directory. It is equivalent to a _git fetch_ and a _git merge_.  


